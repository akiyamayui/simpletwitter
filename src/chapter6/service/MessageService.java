package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public Message select(String messageId){

		Connection connection = null;
		try {
			connection = getConnection();

			/*
			 * idをnullで初期化
			 * ServletからuserIdの値が渡ってきていたら
			 * 整数型に型変換し、idに代入
			 */
			Integer id = null;
			if(!StringUtils.isEmpty(messageId)){
				id = Integer.parseInt(messageId);
			}
			/*
			 * messageDao.selectに引数としてInteger型のidを追加
			 * idがnullだったら全件取得する
			 * idがnull以外だったら、その値に対応するユーザーIDの投稿を取得する
			 */
			Message message = new MessageDao().select(connection, id);
			commit(connection);

			return message;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	public List<UserMessage> select(String firstDate, String secondDate, String userId){
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			String defaultStartDate = "2021-01-01 00:00:00";
			Timestamp startDate;
			Timestamp endDate;

			if(!StringUtils.isEmpty(firstDate)) {
				startDate = Timestamp.valueOf(firstDate + " 00:00:00"); 
			} else {
				startDate = Timestamp.valueOf(defaultStartDate);
			}

			if(!StringUtils.isEmpty(secondDate)) {
				endDate = Timestamp.valueOf(secondDate + " 23:59:59"); 
			} else {
				endDate = new Timestamp(System.currentTimeMillis()); 
			}

			/*
			 * idをnullで初期化
			 * ServletからuserIdの値が渡ってきていたら
			 * 整数型に型変換し、idに代入
			 */
			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			/*
			 * messageDao.selectに引数としてInteger型のidを追加
			 * idがnullだったら全件取得する
			 * idがnull以外だったら、その値に対応するユーザーIDの投稿を取得する
			 */
			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);
			commit(connection);

			return messages;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(String MessageId) {

		Connection connection = null;
		try {
			connection = getConnection();

			Integer id = null;
			if(!StringUtils.isEmpty(MessageId)) {
				id = Integer.parseInt(MessageId);
			} 
			new MessageDao().delete(connection, id);
			commit(connection);

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public void update(Message message, String messageId) {  //つぶやきの編集

		Connection connection = null;
		try {

			connection = getConnection();

			Integer id = null;
			if(!StringUtils.isEmpty(messageId)) {
				id = Integer.parseInt(messageId);
			}

			new MessageDao().update(connection, message,id);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
