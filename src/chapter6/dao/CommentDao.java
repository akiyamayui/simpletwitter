package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Comment;
import chapter6.exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment, Integer id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("  id, ");
			sql.append("  text, ");
			sql.append("  user_id,  ");
			sql.append("  message_id,  ");
			sql.append("  created_date,  ");
			sql.append("  updated_date  ");
			sql.append(") VALUES ( ");
			sql.append("   ?, ");
			sql.append("   ?, ");
			sql.append("   ?, ");
			sql.append("   ?, ");
			sql.append("   CURRENT_TIMESTAMP, ");
			sql.append("   CURRENT_TIMESTAMP ");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1,  comment.getId());
			ps.setString(2, comment.getText());
			ps.setInt(3,  comment.getUserId());
			ps.setInt(4,  id);

			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}