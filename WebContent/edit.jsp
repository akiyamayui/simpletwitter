<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>つぶやき編集画面</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			 <c:if test="${ not empty errorMessages }">
    			<div class="errorMessages">
       				 <ul>
           				 <c:forEach items="${errorMessages}" var="errorMessage">
                			<li><c:out value="${errorMessage}" />
           				 </c:forEach>
        			</ul>
    			</div>
			</c:if>
			<div class="form-area">
				つぶやき<br />
				<form action="edit" method="post">
            		<textarea name="text" cols="100" rows="5" class="tweet-box">${message.text}</textarea>
            		<br />
            		<input type="hidden" name="id" value="${message.id}">
            		<input type="submit" value="更新"><br />
            		<a href="./">戻る</a>
            	</form>
            </div>
			
			 <div class="copyright"> Copyright(c)Akiyama Yui</div>
		</div>
	</body>
</html>